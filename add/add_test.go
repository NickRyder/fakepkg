package add

import (
	"testing"
)

func TestAddGood(t *testing.T) {
	x := 13
	y := 23
	z := Add(x, y)
	if z != x + y {
		t.Errorf("Add returned %d + %d = %d", x, y, z)
	}
}

